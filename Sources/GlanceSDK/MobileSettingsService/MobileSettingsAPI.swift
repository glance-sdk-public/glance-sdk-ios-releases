//
//  MobileSettingsAPI.swift
//  GlanceSDK
//
//  Created by bruno on 12/18/24.
//

import Foundation
#if canImport(GlanceFramework)
import GlanceFramework
#else
import GlanceiOSNetwork
#endif

enum MobileSettingsAPI: APIFetcher {
    case fetchSettings(String, String, String)

    private var baseURL: String {
        return BaseURLs.settings.rawValue
    }

    var path: String {
        var components = URLComponents(string: baseURL)!
        switch self {
        case let .fetchSettings(groupID, platform, visitor):
            components.path += "/v0/settings"
            components.queryItems = [
                URLQueryItem(name: "group_id", value: groupID),
                URLQueryItem(name: "section", value: platform),
                URLQueryItem(name: "section", value: visitor)
            ]
        }

        return components.string ?? ""
    }

    var method: HTTPMethod {
        return .GET
    }

    var task: Codable? {
        return nil
    }

    var header: Codable? {
        return nil
    }

    var debug: Bool {
        return true
    }
}

