//
//  String+Extensions.swift
//  GlanceSDK
//
//  Created by Glance on 23/01/25.
//

import Foundation

extension String {
    func reduceString(toLength maxLength: Int) -> String {
        if self.count > maxLength {
            return String(self.prefix(maxLength))
        } else {
            return self
        }
    }
}
