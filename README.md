# GlanceSDK

The new **GlanceSDK** does not provide any UI, so the major changes are the events being fired by the SDK, so the client can customize their own UI

## Setup

GlanceSDK for iOS is available for installation via SPM.

### SPM

#### Via XCode
1. In Xcode, install **GlanceSDK** by navigating to **File > Add Packages**
2. In the prompt that appears, select the **GlanceSDK** repository:
```
https://gitlab.com/glance-sdk-public/glance-sdk-ios-releases
```
3. Select the version of **glance-sdk-ios-releases** you want to use. For new projects, we recommend using the newest version.

#### Via Package.swift
1. To integrate **GlanceSDK** to a Swift package via a **Package.swift** manifest, you can add **GlanceSDK** to the dependencies array of your package.
```swift
dependencies: [

  .package(name: "GlanceSDK",
           url: "https://gitlab.com/glance-sdk-public/glance-sdk-ios-releases.git",
           from: "6.6.0"),
  // ...

],
```

## Set GlanceDelegate
Implement `GlanceDelegate` protocol
```swift
final class GlanceManager: GlanceDelegate {
.
.
.
}
```

Call `Glance.setDelegate` anywhere in the app to handle **GlanceSDK**'s events':
```swift
Glance.setDelegate(self)
```

### Events
**GlanceDelegate**`s methods will be called everytime something is accomplished, and the UI has to updated according with these events.

The method names gives a good hint of what needs to be done, but they will be covered later in this doc.
```swift
protocol GlanceDelegate {
    func sessionConnected()
    func receivedSessionCode(_ sessionCode: String)
    func sessionAgentConnected()
    func onGuestCountChange(guests: [GlanceAgent], response: String?)
    func sessionEnded(_ sessionKey: String?) 
    func receivedVideoCaptureImage(_ image: UIImage)
    func didStartVideoCapture(_ captureSession: AVCaptureSession)
    func didStopVideoCapture(_ captureSession: AVCaptureSession)
    
    func didPauseVisitorVideo()
    
    func didStartAgentViewer(_ glanceAgentViewer: UIView)
    func didStopAgentViewer()
    
    func onUpdateVisibility(to visibility: Visibility)
    func onUpdateLocation(to location: Location)
}
```

## Configure Session
First of all, call `Glance.initVisitor` to configure the account parameters:
```swift
import GlanceSDK
.
.
let params = GlanceVisitorInitParams()
params.groupid = 2134
params.visitorid = "glance"

Glance.initVisitor(params: params)
```

If your app is using scenes, this must be done after defining your window in `SceneDelegate.swift.
```swift
import GlanceSDK

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?
    var appCoordinator: AppCoordinator?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        .
        .
        .
        window = UIWindow(windowScene: windowScene)
        
        let params = GlanceVisitorInitParams()
        params.groupid = 21489
        params.visitorid = "7899"
        
        Glance.initVisitor(params: params)
    }
    .
    .
    .
}
```
If your app isn't using scenes, you can configure the SDK after defining your window in `AppDelegate.swift`.
```swift
import GlanceSDK

class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    
        window = UIWindow(frame: UIScreen.main.bounds)
        
        let params = GlanceVisitorInitParams()
        params.groupid = 21489
        params.visitorid = "7899"
        
        Glance.initVisitor(params: params)
        .
        .
        .
        return true
    }
    .
    .
    .
}
```
No that you can configure the SDK anywhere in the app, the only requirement is to do it after defining your window.
```swift
.
.
import GlanceSDK
.
.

@main
struct YourApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
                .onAppear {
                    let params = GlanceVisitorInitParams()
                    params.groupid = 21489
                    params.visitorid = "7899"
                    
                    Glance.initVisitor(params: params)
                }
        }
    }
}
```

## Set Video Session
Before calling `Glance.initVisitor`, set `video` parameter to `true`, to configure the video session.
```swift
let params = GlanceVisitorInitParams()
.
.
params.video = true
.
.
```
After starting a video session, these **GlanceDelegate** methods will help you to handle the UI.
```swift
    public func didPauseVisitorVideo() {
        // The Agent has paused visitor video, the visitor AVCaptureSession needs to be paused
    }
    
    public func didStartAgentViewer(_ glanceAgentViewer: UIView) {
        // Receive a UIView that displays the agent video, put this anywhere in your app to display the Agent video recording
    }
    
    public func didStopAgentViewer() {
        // Agents disabled their camera
    }
```

## Change configuration
Note that `initVisitor` can be called many times. If you need to change some information previously configurated. Just change `GlanceVisitorInitParams` and call `initVisitor` again.

The only property that can not be changed is the `groupid`. If you need to change this property it's required to restart the app.

### Parameters
|Parameter                |Type                          |Description                         |
|----------------|-------------------------------|-----------------------------|
|groupid|`Int32`|The account group|
|visitorid|`String`|User id for the Agent to connect with|
|video|`Bool`|Flag to initialize a video session|

## Starting a session
Call `startSession` with desired key.
```swift
let params = GlanceStartParams()
params.key = "8989"

Glance.startSession(params: params)
```
### Parameters
|Parameter                |Type                          |Description                         |
|----------------|-------------------------------|-----------------------------|
|key|`String`            |Session key value            |

### Random session
Set `key` parameter with the value `GLANCE_KEYTYPE_RANDOM`.
```swift
let params = GlanceStartParams()
params.key = "GLANCE_KEYTYPE_RANDOM"

Glance.startSession(params: params)
```

### Number of retries for failed connection on a session
You can set a number of retries in a case of failed connection in `startSession(params:maxConnectAttempts:)`
```swift
let params = GlanceStartParams()
Glance.startSession(params: params, maxConnectAttempts: 4)
```

## Ending a session
```swift
Glance.endSession()
```

## Hosted Sessions
Unlike normal sessions (GlanceVisitor.startSession) initiated by guests/visitors, hosted sessions are initiated by authenticated user (Agents).

### Start Hosted Session
```swift
    GlanceDefaultUI.startHostedSession(username: username, password: password)
```

To start a Hosted Session, you need:
- Agent has to login to Glance Client using a valid Glance account.  
- The guest in this case does not need to log in or have a group id.  The group ID is tied to the agent who logs into the mobile SDK. 
- The ways to join a hosted sessions are: 
    ◦ using the joinssn protocol with the Glance client
    ◦ from the /visitor/join pages (agent video cannot be used in this 2nd case)
    ◦ going directly to a URL like https://www.glance.net/?username=TEST.glance.net&key=1234

### Set delegate to *GlanceHosted*
Set delegate to *GlanceHosted* doing:
```swift
    GlanceHosted.setDelegate(self)
```

### Conform *GlanceHostedDelegate* protocol
For *GlanceHostedDelegate* protocol we have methods you need to conform:
```swift
    hostedSessionDidStart(sessionKey: String)
    hostedSessionDidEnd(errorMessage: String)
```

### Start Hosted Session
```swift
    GlanceHosted.startHostedSession(username: username, password: password)
## Pausing a session

### Pause session
```swift
Glance.pause(true)
```

### Resume session
```swift
Glance.pause(false)
```

### Toggle pause
```swift
Glance.togglePause()
```

### Check pause state
```swift
if Glance.isPaused() {
    ...
}
```

## Presence
GlanceSDK will automaticaly handle presence connection, just need to set if presence is enable or not

### Enable presence
```swift
Glance.setPresence(true)
```

### Number of retries for failed connection on Presence
You can set a number of retries in a case of failed connection in `setPresence(_ isPresenceOn:maxConnectAttempts:)`
```swift
Glance.setPresence(true, maxConnectAttempts: 4)
```

### Disable presence
```swift
Glance.setPresence(false)
```

### Pause Presence
Presence can be paused and resumed by calling the following method. When presence is paused, an agent will not be able to initiate a session using presence until it is resumed.

```swift
Glance.pausePresence(isPaused: True) // Pause session
Glance.pausePresence(isPaused: False) // Resume session
```

## Masking
```swift
Glance.addMaskedView(textField, withLabel: "Masked text field")
Glance.addMaskedView(label, withLabel: "Masked label")
Glance.addMaskedView(button, withLabel: "Masked button")
```

## Masking Keyboard
```swift
Glance.maskKeyboard(true)
```

## Masking in SwiftUI
```swift
Text("Label to be masked")
    .addMaskedView(withLabel: "Masked text")
```

## Remove Masking
```swift
Glance.removeMaskedView(textField)
```

## WebMasking

To support masking inside a webview, you will need to make some changes to the way your webview is setup.

You will need to create an instance of class `GlanceMaskContentController`. When you create this class you can pass in the maskingQuerySelector and maskingLabels that the webView should mask. This class should then be set as the userContentController for the webView. For reference, review the sample code below:

`maskingQuerySelectors` defines an array of CSS selectors (like .mask_1, #mask_3, etc.) that target elements on the webpage.
`maskingLabels` defines an array of labels which will be displayed in lieu of the masked content so that the agent can recognize what type of data is being masked. ( maskingLabels are optional )

```swift
let maskingQuerySelectors = [".mask_1", ".mask_2", "#mask_3", ".mask_4", "span", "#hplogo"]
let maskingLabels = ["mask 1", "mask 2", "mask 3", "mask 4", "span", "LOGO"]
let glanceMaskContentController = GlanceMaskContentController(
    maskingQuerySelectors.joined(separator: ", "),
    labels: maskingLabels.joined(separator: ", ")
)
let config = WKWebViewConfiguration()
config.userContentController = glanceMaskContentController

let maskingWebView = WKWebView(frame: self.view.bounds, configuration: config)
maskingWebView.load(URLRequest(url: URL(string: "https://d2e93a2oavc15x.cloudfront.net")!))
glanceMaskContentController.setWebView(maskingWebView)
self.view.addSubview(maskingWebView)
```

For the list of query selectors, you can pass in anything that will be found via the document.querySelectorAll() javascript method. When the webview is loaded, the SDK will search for the given query selectors, find it's location on the screen, apply a mask over the entire element, and apply a label on top of the mask, if provided.


The following selectors will be masked by default:

Any element with the class `glance_maske`

Any element with the attribute `glance_masked="true"`

Any text input element with `type="password"`

On the agent side, the webview will be paused until it is fully loaded and the masks have been added. The webview will also pause when the user is scrolling within the webview, allowing the masks to be repositioned without the agent seeing any of the contents underneath.

This implementation also supports webpage mutations, along with masking within iframes.

## Start visitor preview
**GlanceSDK** builds a `AVCaptureSession` to be used to display a preview from the visitor's camera, call `Glance.startVideoPreview`.
```swift
Glance.startVideoPreview()
```
After starting the preview, these events will handle it's life cycle. Make sure to implement *GlanceDelegate* protocol.
```swift
final class GlanceManager: GlanceDelegate {
.
.
.
    // In case you want to use pictures of AVCaptureSession
    public func receivedVideoCaptureImage(_ image: UIImage) {
        sessionCoordinator.onUpdateImage.send(image)
    }

    // This will be called when visitor preview start, and provides an AVCaptureSession object to be used anywhere in your app
    public func didStartVideoCapture(_ captureSession: AVCaptureSession) {
        
    }

    // This will be called when visitor preview stop 
    public func didStopVideoCapture(_ captureSession: AVCaptureSession) {
        
    }
.
.
.
}
```
## Stop visitor preview
This needs to be called when you don't need visitor preview anymore, probably when leaving the screen where this feature is necessary. This will improve your app's performance.
```swift
Glance.stopVideoPreview()
```

## Agent Info and Dynamic Masking in the iOS SDK

### Accessing Agent Information

Your application will receive agent information ( also referred to as guests ) through the onGuestCountChange method, which can be implemented as part of the GlanceDelegate interface.

This method is triggered whenever an agent joins or leaves a session. Notably, the onGuestCountChange method is not invoked when the last agent exits; instead, the session ends.

In addition to the array of GlanceAgents, the raw json string is included as an optional parameter if direct access to the response data is preferred.

```swift
extension GlanceManager: GlanceDelegate {
    ...
    public func onGuestCountChange(guests: [GlanceAgent], response: String?)
        // Guest count changed
        // guests has the list of guests( agents ) see GlanceAgent class for details
        // response has the raw server response
    }
    ...
}

```

### Structure of Guest List

The guest list is accessible through two distinct methods:

1. JSON Array: It can be retrieved from the response parameter as a string representing a JSON array.

2. GlanceAgents Array: Alternatively, the guest list is available through the guests parameter, which is an array of GlanceAgents. Each GlanceAgent object serves as a serialization of the JSON string.

*Note: The order of guests in both the GlanceAgent array and the JSON string corresponds to the order in which agents join the session, with the first agent to join appearing first.*

The following declaration outlines the structure of both the JSON string and the GlanceAgent array:

```swift

// Example of JSON String: 
// [ {"role":"visitor"},{"agentrole":"Supervisor","name":"Dr. Rich Baker","partnerid":12345,"partneruserid":"rbaker","role":"agent","title":"Gentleman + Scholar","username":"rich.glance.net"} ]

@objc public class GlanceAgent: NSObject, Decodable {

    public var agentRole: String?
    public var name: String?
    public var partnerId: Int?
    public var partnerUserId: String?
    public var role: String?
    public var title: String?
    public var username: String?

    enum CodingKeys: String, CodingKey {
        case agentRole = "agentrole"
        case name
        case partnerId = "partnerid"
        case partnerUserId = "partneruserid"
        case role
        case title
        case username
    }
}
```

**Note:**
- *Role* refers to the participant's role, which can be "visitor", "agent", "guest", or "host".
- *Agentrole* is the name of any Role set up for Role-based permissions.

By default, accounts only transmit the participant's *role* and *agentrole* for agents. However, you can configure accounts to send additional agent information like name, username, and partneruserid.

Utilizing agent information in the guest list can aid in implementing masking based on the agent's role, username, or partnerxuserid.

### Role-based Masking

The agent information passed in the guestlist can be useful for implementing masking based on agent role, username or partneruserid.
You will need to handle any masking or any customization of your application you wish to do based on agents or agent roles.
The code below will mask two fields if any agent with role "mask" joins.  If only agents in other roles join, there will be masking:

```swift

 func _handleGuestList(_ guests: [Any]) {
        var masked: Bool = false;
        for object in guests {
            if let guest = object as? [String: Any] {
                if let role = guest["role"] as? String {
                    if (role == "agent") {
                        if let agentrole = guest["agentrole"] as? String {
                            // here is where we check for role-based masking
                            if (agentrole == "mask") { masked = true }
                        } else {
                            print("agentrole missing from guestlist agent")
                        }
                    }
                } else {
                    print("guest role missing")
                }
            }
        }
        
        // here is where the actual role-based masking happens
        if (masked) {
            Glance.addMaskedView(groupId)
            Glance.addMaskedView(sessionKey)
        } else {
            Glance.removeMaskedView(groupId)
            Glance.removeMaskedView(sessionKey)
        }
    }
```

## Rendering Agent View

There are two main ways the SDK can draw the screen for the Agent View: renderInContext and drawViewInHierarchy.

RenderInContext can have a performance improvement over drawViewInHierarchy, but it also has the limitation that some elements of the screen will not be drawn for the agent. The main elements that are not currently supported by RenderInContext are alertView backgrounds, tabBar backgrounds, and Flutter views. 

By Default, RenderInContext is used during app startup.

If your app uses Flutter or you want alert views to show completely, you can change the renderMethod using the following method:

	Glance.setDrawViewHierarchy( true )

To use renderInContext instead, you should use:
 
        Glance.setDrawViewHierarchy( false ) 

Note: You can switch between these two render methods during a session, so if you prefer renderInContext for performance boosts then you can use drawViewHierarchy only when needed.

These two methods have different performance times based on the exact screen composition that each is rendering, so in some cases renderInContext will give a performance improvement and in other cases drawViewHierarchy will. It depends on your exact screen and the view hierarchy contained within it. In general, for simpler screens containing basic UI elements, renderInContext should be the preferred method, but for more complicated screens DrawViewHierarchy should be used.


## Responding to Agent Events

During an active session, the agent has a series of controls which allows them to move, collapse, and expand the widget shown on the client’s screen as well as start video. 

The SDK provides methods which are invoked when these events are received from the server ( triggered by Agent’s actions ). These methods can update the UI to respond accordingly, but are not required.

For example, if video is not desired to be displayed even if the agent tries to launch video, the app should not respond to the video delegate methods that are triggered and not take any actions in updating the UI. 

### Agent Portal Controls

When an agent is in a session they have the option to turn their camera on or off which will start video as well as the ability to toggle the widget size and location displayed in the visitor's view. 

Toggling the widget from Full to Tab will collapse the tab on the device, and tapping in any of the corners will move the widget to one of the corners of the screen. This is the behavior when using the DefaultUI, but as mentioned earlier, these events from the server can be implemented ( or not ) by any custom UI implementation.

### Video Delegate Methods

The following methods are used when displaying agent video and agent video requests.

####    public func presentAgentAskShareVideoDialog()

This method is called when an agent tries to request video from a user. This method is invoked depending on the session conditions, and is not always available to the agent.

####    public func didStartAgentViewer(_ glanceAgentViewer: UIView) 

When an agent starts sharing video of themselves, this method is called where glanceAgentViewer is a UIView displaying the video of the Agent
 
#### public func didStopAgentViewer() 

When an agent stops sharing video of themselves, this method is called. The previous UIView sent in didStartAgentViewer should not be displayed anymore.

#### public func onUpdateSize(size: String)

When an agent presses the full screen/minimize icon in the corner of their video, it will trigger this method on the device. The app should respond by increasing/decreasing the video view. Size is either “large” or “small”.

### Widget Location & Appearance Methods
The following methods are used when responding to agent requests to collapse , expand, and change the location of the widget.

####  public func onUpdateLocation(to location: Location)

When an agent presses any of the corners of the Location controls on their display, this method will be triggered and the location should be updated. Location can be either: `topleft`, `topright`, `bottomleft`, `bottomright`. 

    @objc public enum Location: NSInteger {
        case topLeft = 1
        case topRight = 2
        case bottomLeft = 3
        case bottomRight = 4
    
        static func getLocation(from location: String) -> Location? {
            switch location {
            case "topleft": return .topLeft
            case "topright": return .topRight
            case "bottomleft": return .bottomLeft
            case "bottomright": return .bottomRight
            default: print("Must handle new location case.") return nil
            }
        } 
    }

#### public func onUpdateVisibility(to visibility: Visibility)

When an agent presses the switch to change between full screen and tab, this method will be triggered and the widget should be updated. Visibility can be either tab or full.

    @objc public enum Visibility: NSInteger {
        case tab = 1
        case full = 2
    }


## Terms
**GlanceSDK** dosen't handle terms, this must be done within your own app.

## Tracking Session Information

Each session is given an unique callId, this callId can be accessed by calling Glance.getCallId(). 

## Send messages
If you need to send any messages to the SDK, call `Glance.send`.
```swift
Glance.send(message: message, properties: properties)
```
|Parameter                |Type                          |Description                         |
|----------------|-------------------------------|-----------------------------|
|message|`String`            |message id            |
|properties|`String`            |message properties concatenated as a String            |

## All events
```swift
extension GlanceManager: GlanceDelegate {
    public func sessionConnected() {
        // Connected to a session, but still waiting for the Agent to connect
    }
    
    public func receivedSessionCode(_ sessionCode: String) {
        // Receive a session code to be sent to the agent
    }
    
    public func sessionAgentConnected() {
        // Agent connected to the session
    }
    
    public func onGuestCountChange(guests: [GlanceAgent], response: String?) {
        // Guest count changed
        // glanceAgent has the agent's information

    }
    
    public func sessionEnded(_ sessionKey: String?) 
        // Session ended
        // sessionKey is the key of the session that just ended.
    }
    
    public func receivedVideoCaptureImage(_ image: UIImage) {
        // Current image being recorded on the iPhone's camera
    }
    
    public func didStartVideoCapture(_ captureSession: AVCaptureSession) {
        // Visitor iPhone's camera started recording
    }
    
    public func didStopVideoCapture(_ captureSession: AVCaptureSession) {
        // Visitor iPhone's camera stopped recording
    }
    
    public func didPauseVisitorVideo() {
        // The Agent has paused visitor video
    }
    
    public func didStartAgentViewer(_ glanceAgentViewer: UIView) {
        // Receive a UIView that displays the agent video
    }
    
    public func didStopAgentViewer() {
        // Agents disabled their camera
    }
    
    public func onUpdateVisibility(to visibility: Visibility) {
        // Agent has updated widget visibility to another state
    }
    
    public func onUpdateLocation(to location: Location) {
        // Agent has updated widget location to one of the 4 courners in the screen
    }
}
```

## Default Integration Settings

You can define default configurations for three features before fetching any server‐side settings. These defaults apply when the server does not explicitly provide an override.

### 1. isDefaultPresenceIntegrationEnabled
If `true`, Presence is assumed to be enabled unless the server says otherwise.

### 2. isDefaultTwoWayVideoIntegration
If `true`, two‐way video is assumed to be enabled by default.

### 3. isDefaultRenderModeIntegration
If `true`, rendering uses `drawViewInHierarchy` by default; if `false`, it uses `renderInContext`.

### Usage Example
```swift
Glance.initVisitor(
    params: params,
    fetchSettingsInterval: .never, // or any other interval
    isDefaultPresenceIntegrationEnabled: true,
    isDefaultTwoWayVideoIntegration: false,
    isDefaultRenderModeIntegration: false
)
```
These values will serve as defaults until (and unless) `Glance.fetchSettings` returns new values that override them. If no overrides are returned, the defaults remain in effect.

### SettingsInterval Enum
Additionally, the SDK provides a `SettingsInterval` enum to control how often `Glance.fetchSettingsIfNeeded` is called:
- `.never` — Never fetch remote settings after the first load.
- `.always` — Always fetch whenever fetchSettingsIfNeeded is called.
- `.repeating(Int)` — Fetch if the specified number of seconds has elapsed since the last fetch.

## Measurements
| Memory Usage (MB) | Build Time (milliseconds) | Size (MB) |
| --- | --- | --- |
| 37.4 MB | 700ms | 146 MB |

### Init & Session without Video

| Action                               | Memory Usage | Progress |
| --- | --- | --- |
| Init app                             | 31.8 MB      | 16%      |
| Glance SDK Init (with Presence)      | 34.1 MB      | 76%      |
| Start session through Presence      | 40 MB        | 40%      |
| Agent Joins Screen Share Session    | 44.1 MB      | 16%      |
| During session, navigate to a new screen | 44.1 MB  | 7%       |
| End session                          | 40 MB        | 4%       |

### Init & Session with Video

| Init app                                | 31.8 MB     | 16%   |
| Glance SDK Init (with Presence)         | 34.1 MB     | 76%   |
| Start session through Presence         | 40 MB       | 40%   |
| Agent Joins Screen Share Session with Video | 49 MB    | 48%   |
| During session, navigate to a new screen | 47 MB      | 47%   |
| During session, move video to tab      | 47 MB       | 37%   |
| End session                             | 40 MB       | 4%    |



## Webserver Codes

The Glance webserver may return the following messages or error codes to the SDK during usage. You may see them in logging while integrating the SDK.

#### Connection Messages
| Name | Value | Built-in Descriptions | Explanation |
| --- | --- | --- | --- |
| OK | SWAP16(0x0000) | - | Connection is established successfully |
| FAIL | SWAP16(0x1000) | "Invalid session id"	<br> "Invalid client info" <br> "Duplicate display connect" <br> "Invalid protocol verion for continuous updates" | Connection failed because of invalid data. |
| FAIL_VERSION | FAIL + SWAP16(0x0001) | - | Connection falied because versions are incompatible. |
| FAIL_UNAUTHORIZED| FAIL + SWAP16(0x0002) | "Not authorized to join this session"	<br> "Start screenshare not allowed"<br>"Unauthorized"<br>"Not authorized to join this session"<br>"Server key required" | Connection failed because of authorization problems. |
| FAIL_NO_SESSION | FAIL + SWAP16(0x0003) | "The session has ended" | Connection failed because the requested session has ended. |
| FAIL_SSN_FULL | FAIL + SWAP16(0x0005) | - | Session serial number problem. |

#### Close Messages
Name | Value | Description
--- | --- | ---
QUIT | SWAP16(0x0000) | Session ended.
TIMED_OUT | SWAP16(0x0001) | timed out waiting for the other side.
SERVER_ERROR | SWAP16(0x0002) | Server is closing because it errored.
CLIENT_ERROR | SWAP16(0x0003) | Client is closing because it errored.
PEER_ERROR | SWAP16(0x0004) | Other side closed because it errored.
RESET | SWAP16(0x0005) | Client sent something invalid.
RECONNECT | SWAP16(0x0006) | Client should re-connect to another server.
SERVER_KILLED | SWAP16(0x0007) | Server killed session intentionally.
DECLINED | SWAP16(0x0008) | Guest declined to share their screen.
CONN_DEAD | SWAP16(0x0009) | Connection timed out due to inactivity.
CALL_DEAD | SWAP16(0x000A) | Call timed out due to inactivity.
REPLACED | SWAP16(0x000B) | A guest opened a second viewer.
INFO_SHUTDOWN | SWAP32(0x0001) | Closing due to system shutfdown or logoff.
