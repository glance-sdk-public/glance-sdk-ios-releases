//
//  MessageManager.swift
//
//
//  Created by Felipe Melo on 22/11/23.
//

import Foundation

public enum Message: String {
    case visitorPaused = "visitorvideopaused"
    case widgetLocation = "widgetlocation"
    case widgetVisibility = "widgetvisibility"
    case videoSize = "videosize"
}

public protocol MessageManagerType {
    func pause(_ isPaused: Bool)
    func widgetLocationUpdated(_ location: String)
    func widgetVisibilityUpdated(_ visibility: String)
    func visitorSizeUpdated(_ size: VideoSizeMessage)
}

public enum VideoSizeMessage: String {
    case large
    case small
}

final public class MessageManager: MessageManagerType {
    
    public init() {}
    
    public func pause(_ isPaused: Bool) {
        let properties = "message:\(Message.visitorPaused.rawValue);paused:\(isPaused)"
        Glance.send(message: Message.visitorPaused.rawValue, properties: properties)
    }
    
    public func widgetLocationUpdated(_ location: String) {
        let properties = "message:\(Message.widgetLocation.rawValue);location:\(location)"
        Glance.send(message: Message.widgetLocation.rawValue, properties: properties)
    }
    
    public func widgetVisibilityUpdated(_ visibility: String) {
        let properties = "message:\(Message.widgetVisibility.rawValue);visibility:\(visibility)"
        Glance.send(message: Message.widgetVisibility.rawValue, properties: properties)
    }
    
    public func visitorSizeUpdated(_ size: VideoSizeMessage) {
        let properties = "message:\(Message.videoSize.rawValue);size:\(size.rawValue);previewwidth:95;previewheight:95"
        Glance.send(message: Message.widgetVisibility.rawValue, properties: properties)
    }
}
