//
//  Glance+MobiletSettings.swift
//  GlanceSDK
//
//  Created by bruno on 12/19/24.
//

import Foundation
import GlanceCore
import Combine

extension Glance {
    public enum SettingsInterval: Equatable {
        case never
        case always
        case repeating(Int)
    }

    /// Public-facing entry point with completion
    public static func fetchSettingsIfNeeded(
        groupID: String,
        platform: String,
        visitor: String,
        completion: @escaping () -> Void = {}
    ) {
        shared.fetchSettingsIfNeeded(
            groupID: groupID,
            platform: platform,
            visitor: visitor,
            completion: completion
        )
    }

    /// Internal helper with completion
    func fetchSettingsIfNeeded(
        groupID: String,
        platform: String,
        visitor: String,
        completion: @escaping () -> Void
    ) {
        // If we never fetched before, fetch immediately
        guard let lastFetch = lastFetchDate else {
            fetchSettings(
                groupID: groupID,
                platform: platform,
                visitor: visitor,
                completion: completion
            )
            lastFetchDate = Date()
            return
        }
        
        switch fetchSettingsInterval {
        case .never:
            // Never fetch, just call completion
            completion()
            
        case .always:
            // Always fetch, then call completion
            fetchSettings(
                groupID: groupID,
                platform: platform,
                visitor: visitor,
                completion: completion
            )
            lastFetchDate = Date()
            
        case let .repeating(seconds):
            if let lastFetch = lastFetchDate,
               Date().timeIntervalSince(lastFetch) >= Double(seconds) {
                fetchSettings(
                    groupID: groupID,
                    platform: platform,
                    visitor: visitor,
                    completion: completion
                )
                lastFetchDate = Date()
            } else {
                // Not time yet, just invoke completion
                completion()
            }
        }
    }

    /// Actual network/service call
    private func fetchSettings(
        groupID: String,
        platform: String,
        visitor: String,
        completion: @escaping () -> Void
    ) {
        service
            .fetchSettings(groupID: groupID, platform: platform, visitor: visitor)
            .map(\.results)
            .catch { [weak self] _ in
                // handle errors as needed, but still call completion
                return Just(nil).eraseToAnyPublisher()
            }
            .receive(on: DispatchQueue.main)
            .sink { [weak self] newSettings in
                self?.mobileSettings = newSettings
                completion()
            }
            .store(in: &cancellables)
    }
}
