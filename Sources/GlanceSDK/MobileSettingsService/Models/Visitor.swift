//
//  Visitor.swift
//  GlanceSDK
//
//  Created by bruno on 12/19/24.
//

struct Visitor: Codable {
    let VServer: String
    let coloSelectUrl: String
    let isMetricsEnabled: Bool
    let isPresenceLoadBalanced: Bool
    let metricsPassword: String
    let metricsUrl: String
    let presenceUrl: String
    let privilegeString: String
    let screenshareTermsUrl: String
    let service: Int
}
