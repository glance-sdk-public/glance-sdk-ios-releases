//
//  GlanceSDKEvent.swift
//
//
//  Created by bruno on 5/10/24.
//

import GlanceCore

public enum EventCode: Int {
    case EventNone
    case EventInvalidParameter
    case EventInvalidState
    case EventLoginSucceeded
    case EventLoginFailed
    case EventPrivilegeViolation
    case EventInvalidWebserver
    case EventUpgradeAvailable
    case EventUpgradeRequired
    case EventCompositionDisabled
    case EventConnectedToSession
    case EventSwitchingToView
    case EventStartSessionFailed
    case EventJoinFailed
    case EventSessionEnded
    case EventFirstGuestSession
    case EventTunneling
    case EventRestartRequired
    case EventConnectionWarning
    case EventClearWarning
    case EventGuestCountChange
    case EventViewerClose
    case EventActionsChange
    case EventDriverInstallError
    case EventRCDisabled
    case EventDeviceDisconnected
    case EventDeviceReconnected
    case EventGesture
    case EventException
    case EventScreenshareInvitation
    case EventMessageReceived
    case EventChildSessionStarted
    case EventChildSessionEnded
    case EventJoinChildSessionFailed
    case EventVisitorInitialized
    case EventPresenceConnected
    case EventPresenceConnectFail
    case EventPresenceShowTerms
    case EventPresenceStartSession
    case EventPresenceSignal
    case EventPresenceBlur
    case EventPresenceSendFail
    case EventPresenceNotConfigured
    case EventPresenceDisconnected
    case EventPresenceStartVideo
    case EventVisitorVideoRequested
    
    public init(fromRawValue: UInt) {
        self = .init(rawValue: Int(fromRawValue)) ?? .EventNone
    }
}

public enum EventType: Int {
    case none
    case info
    case warning
    case error
    case assertFail
    
    public init(fromRawValue: UInt) {
        self = .init(rawValue: Int(fromRawValue)) ?? .none
    }
}

public struct GlanceSDKEvent {
    public var code: EventCode
    public var type: EventType
    public var message: String
    public var properties: [AnyHashable: Any]

    public init(
        code: EventCode,
        type: EventType,
        message: String,
        properties: [AnyHashable: Any]
    ) {
        self.code = code
        self.type = type
        self.message = message
        self.properties = properties
    }
    
    public init(_ object: GlanceEvent) {
        self.code = .init(fromRawValue: object.code.rawValue)
        self.type = .init(fromRawValue: object.type.rawValue)
        self.message = object.message
        self.properties = object.properties
    }
}
