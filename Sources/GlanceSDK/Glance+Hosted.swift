//
//  GlanceHosted.swift
//  GlanceSDK
//
//  Created by bruno on 12/2/24.
//

import Foundation
import GlanceCore

extension Glance {
    public static func setHostedSessionDelegate(_ delegate: GlanceHostedDelegate) {
        shared.hostedSessionDelegate = delegate
    }
    
    public static func startHostedSession(username: String, password: String) {
        shared.startHostedSession(username: username, password: password)
    }
    
    public static func stopHostedSession() {
        shared.stopHostedSession()
    }
    
    func startHostedSession(
        username: String,
        password: String
    ) {
        if hostedSession != nil {
            stopHostedSession()
        } else {
            hostedUser = GlanceUser()
            hostedUser?.onEventNotifyTarget(self, selector: #selector(onHostedUserEvent(_:)))
            hostedUser?.authenticate(withUsername: username, password: password)
        }
    }
    
    func stopHostedSession() {
        hostedSession?.end()
        hostedGuestSession?.end()
    }
    
    @objc
    func onHostedUserEvent(_ event: GlanceEvent) {
        switch event.code {
        case EventLoginSucceeded:
            print("Login succeeded")
            hostedSessionAuthenticated()
            
        case EventLoginFailed:
            debugPrint("Login failed: \(event.message)")
            
        case EventStartSessionFailed:
            debugPrint("Start session failed: \(event.message)")
            hostedSessionDidEnd(event.message)
            
        default:
            debugPrint("Glance user event \(event.code), \(event.message ?? "No message")")
        }
    }
    
    func hostedSessionAuthenticated() {
        self.hostedSession = GlanceHostSession(user: self.hostedUser)
        
        self.hostedSession?.onEventNotifyTarget(self, selector: #selector(onHostedSessionEvent(_:)))
        
        let startParams: GlanceStartParams = GlanceStartParams()
        startParams.key = String(format: "%04d", Int.random(in: 0..<10000))
        
        self.hostedSession?.show(startParams)
        self.hostedSession?.maskKeyboard(true)
    }
    
    @objc
    func onHostedSessionEvent(_ event: GlanceEvent) {
        switch event.code {
        case EventConnectedToSession:
            hostedSessionDidConnect()
            break;
            
        case EventGuestCountChange:
            debugPrint("host session EventGuestCountChange")
            break;
            
        case EventInvalidParameter,
            EventInvalidState,
            EventInvalidWebserver,
        EventPrivilegeViolation:
            debugPrint("Invalid operation: \(event.message)")
            hostedSessionDidEnd(event.message)
            break;
            
        case EventStartSessionFailed:
            debugPrint("Start session failed (on Session!): \(event.message)")
            hostedSessionDidEnd(event.message)
            break;
            
        case EventSessionEnded:
            hostedSessionDidEnd()
            break;
            
        case EventScreenshareInvitation:
            if let username = event.properties["username"] as? String,
               let sessionKey = event.properties["sessionkey"] as? String {
                
                hostedGuestSession = GlanceGuestSession()
                hostedGuestSession?.onEventNotifyTarget(self, selector: #selector(onHostedGuestEvent(_:)))
                //                hostedGuestSession?.setCustomViewerDelegate(self)
                hostedGuestSession?.contentMode = .scaleAspectFill
                
                if event.properties["sessiontype"] as? String != "agentVideo" {
                    let joinParams: GlanceJoinParams = GlanceJoinParams()
                    joinParams.decline = true
                    hostedGuestSession?.join(username, key: sessionKey, joinParams: joinParams)
                    break
                }
                
                DispatchQueue.main.async {
                    self.hostedGuestVideoJoin(username: username, key: sessionKey, ntries: 1)
                }
            }
            break;
        default:
            break;
        }
    }
    
    @objc
    func onHostedGuestEvent(_ event: GlanceEvent) {
        switch event.code {
        case EventConnectedToSession:
            break;
            
        case EventInvalidParameter,
            EventInvalidState,
            EventInvalidWebserver,
        EventPrivilegeViolation:
            debugPrint("Invalid operation: \(event.code)")
            self.hostedGuestVideoSessionDidEnd()
            break;
            
        case EventStartSessionFailed: // This happens on User not Session
            self.hostedGuestVideoSessionDidEnd()
            debugPrint("Start session failed (on Session!): \(event.message)")
            break;
            
        case EventSessionEnded:
            self.hostedGuestVideoSessionDidEnd()
            break;
            
        default:
            debugPrint("guest event \(event.code)")
            break;
        }
    }
    
    func hostedSessionDidConnect() {
        DispatchQueue.main.async { [weak self] in
            guard let self else { return }
            if let sessionInfo = self.hostedSession?.getInfo(),
               let sessionKey = sessionInfo.key as? String {
                self.hostedSessionDelegate?.hostedSessionDidStart(sessionKey: sessionKey)
                debugPrint("Session Key: \(sessionKey)")
            }
        }
    }
    
    func hostedSessionDidEnd(_ errorMessage: String = "") {
        DispatchQueue.main.async { [weak self] in
            guard let self else { return }
            self.hostedSession = nil
            self.hostedSessionDelegate?.hostedSessionDidEnd(errorMessage: errorMessage)
            debugPrint("Session Ended")
        }
    }
    
    func hostedGuestVideoJoin(username: String, key: String, ntries: Int) {
        guard let hostedGuestSession = self.hostedGuestSession else { return }
        
        guard let sessionInfo = hostedGuestSession.lookup(username, key: key) else { return }
        
        if sessionInfo.callId == 0 { // check if video session is still running
            if ntries > 3 {
                self.hostedGuestSession = nil
                return
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                self.hostedGuestVideoJoin(username: username, key: key, ntries: ntries + 1)
            }
            return
        }
        
        let joinParams: GlanceJoinParams = GlanceJoinParams()
        joinParams.guestName = "ios"
        if sessionInfo.isReverse {
            joinParams.decline = true
        }
        
        hostedGuestSession.join(username, key: key, joinParams: joinParams)
    }
    
    func hostedGuestVideoSessionDidEnd() {
        debugPrint("video session ended")
        self.hostedGuestSession = nil
    }
}
