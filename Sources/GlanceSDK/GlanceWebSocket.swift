//
//  GlanceWebSocket.swift
//
//
//  Created by Felipe Melo on 19/01/24.
//

import Foundation
import GlanceCore

actor GlanceWebSocket: NSObject {
    
    public var isConnected = false
    public var firstConnection = true
    
    private var webSocket : URLSessionWebSocketTask?
    private var initParams: GlanceVisitorInitParams?
    private var receiveThread: DispatchWorkItem?
    private var receiveTask: Task<Void, Never>?
    private var reconnectTask: Task<Void, Never>?
    
    func setup(startParams: GlanceVisitorInitParams) async {
        self.initParams = startParams
        
        await disconnect()
        await connect()
    }
    
    func connect() async {
        guard let presenceServer = GlancePresenceVisitor.getPresenceServer(),
              !presenceServer.isEmpty else { return }
        
        //Session
        let session = URLSession(configuration: .default, delegate: self, delegateQueue: OperationQueue())
        
        //Server API
        if let url = URL(string: "wss://\(presenceServer)/visitorws") {
            //Socket
            webSocket = session.webSocketTask(with: url)
            
            //Connect and handles handshake
            webSocket?.resume()
        }
    }
    
    func disconnect() async {
        receiveThread?.cancel()
        receiveThread = nil
        
        await closeSession()
        webSocket = nil
        isConnected = false
        
        reconnectTask?.cancel()
        reconnectTask = nil
        
        firstConnection = true
    }
    
    func reconnect() {
        // Cancel any existing reconnect task
        reconnectTask?.cancel()
        
        // Create and store the new task
        reconnectTask = Task.detached { [weak self] in
            guard let self = self else { return }
            
            // Sleep for 10 seconds (10_000_000_000 nanoseconds)
            try? await Task.sleep(nanoseconds: 10_000_000_000)
            
            // Check for cancellation
            guard !Task.isCancelled else { return }
            
            // Check if still not connected and attempt to reconnect
            if await !self.isConnected {
                await self.reconnect()
            }
        }
    }
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    func updateFirstConnectionStatus(firstConnection: Bool) {
        self.firstConnection = firstConnection
    }
    
    func handleConnection() {
        isConnected = true
        reconnectTask?.cancel()
        receive()
        send(message: .connect)
    }
    
    func handleCloseConnection() {
        if isConnected {
            isConnected = false
            reconnect()
        }
    }
}

extension GlanceWebSocket: URLSessionWebSocketDelegate {
    func receive(){
        /*
         - Create a workItem
         - Add it to the Queue
         */
        
        receiveTask = Task { [weak self] in
            guard let self else { return }
            await self.webSocket?.receive(completionHandler: { [weak self] result in
                guard let self else { return }
                Task {
                    let firstConnection = await self.firstConnection
                    let isConnected = await self.isConnected
                    if firstConnection && isConnected {
                        Task { @MainActor in
                            await self.updateFirstConnectionStatus(firstConnection: false)
                        }
                    }
                    
                    switch result {
                    case .success(let message):
                        
                        switch message {
                            
                        case .data(let data):
                            print("Data received \(data)")
                            
                        case .string(let strMessage):
                            print("String received \(strMessage)")
                            var dict = await self.convertToDictionary(text: strMessage)
                            
                            var data = ""
                            if let data1 = (dict?["data"] as? [String: String])?.description {
                                data = data1
                            } else if var data2 = (dict?["data"] as? [String: Any]) {
                                let args = (data2["args"] as? [String: String])?.description
                                data2["args"] = args
                                data = data2.description
                            }
                            data = data.replacingOccurrences(of: "[", with: "{")
                            data = data.replacingOccurrences(of: "]", with: "}")
                            dict?["data"] = data
                            
                            if let type = dict?["type"] {
                                dict?["mtype"] = type
                            }
                            
                            GlancePresenceVisitor.handleMessage(dict)
                            
                        default:
                            break
                        }
                        
                    case .failure(let error):
                        return
                    }
                    
                    await self.receive()
                }
            })
        }
    }
    
    //MARK: Send
    func send(message: WSMessage, data: [String: String] = [:]) {
        if let initParams = self.initParams {
            let data = Data(message.buildMessage(initParams: initParams, data: data).utf8)
            let wsMessage = URLSessionWebSocketTask.Message.data(data)
            self.webSocket?.send(wsMessage, completionHandler: { error in
                if error != nil {
                    print(error)
                }
            })
        }
    }
    
    //MARK: Close Session
    @objc func closeSession() async {
        webSocket?.cancel(with: .goingAway, reason: "You've Closed The Connection".data(using: .utf8))
    }
    
    //MARK: URLSESSION Protocols
    
    nonisolated func urlSession(_ session: URLSession, webSocketTask: URLSessionWebSocketTask, didOpenWithProtocol protocol: String?) {
        Task {
            await handleConnection()
        }
    }
    
    nonisolated func urlSession(_ session: URLSession, webSocketTask: URLSessionWebSocketTask, didCloseWith closeCode: URLSessionWebSocketTask.CloseCode, reason: Data?) {
        Task {
            await handleCloseConnection()
        }
    }
}

enum WSMessage: String {
    case connect
    case terms
    case visitorsessionstart
    
    private var version: String {
        #if canImport(GlanceSDK)
            Bundle.main.object(forInfoDictionaryKey: "AppVersion") as? String ?? "Unknown"
        #else
            Bundle(for: GlanceWebSocket.self).object(forInfoDictionaryKey: "AppVersion") as? String ?? "Unknown"
        #endif
    }
    
    func buildMessage(initParams: GlanceVisitorInitParams, data: [String: String] = [:]) -> String {
        guard let visitorid = initParams.visitorid else { return "" }
        var message = ""
        switch self {
        case .terms:
            message = "{\"groupid\":\(initParams.groupid), \"type\": \"\(self.rawValue)\", \"version\":\"\(version)\", \"visitorid\":\"\(visitorid)\""
        default:
            message = "{\"groupid\":\(initParams.groupid), \"type\": \"\(self.rawValue)\", \"version\":\"\(version)\", \"visitorid\":\"\(visitorid)\""
        }
        
        if !data.isEmpty {
            var data = data.description
            data = data.replacingOccurrences(of: "[", with: "{")
            data = data.replacingOccurrences(of: "]", with: "}")
            
            message += ", \"data\":\(data.description)"
        }
        
        message += "}"
        
        return message
    }
}
