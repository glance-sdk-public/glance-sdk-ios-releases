//
//  Glance.swift
//
//
//  Created by Felipe Melo on 21/07/23.
//

import AVFoundation
import GlanceCore
import Combine

@objc public class GlanceError: NSObject {
    public let code: EventCode
    public let message: String
    
    public init(code: EventCode, message: String) {
        self.code = code
        self.message = message
    }
}

public final class GlanceTimeout {
    public enum TimeoutType {
        case `default`, network
    }
    
    public var timeInSeconds: Double
    public var type: TimeoutType
    
    public init(timeInSeconds: Double, type: TimeoutType) {
        self.timeInSeconds = timeInSeconds
        self.type = type
    }
}

@objc public enum GlanceVisibility: Int, CustomStringConvertible {
    case tab = 1
    case full = 2
    
    public var description: String {
        switch self {
        case .tab:
            "tab"
        case .full:
            "full"
        }
    }
}

@objc public enum GlanceLocation: Int, CustomStringConvertible {
    case topLeft = 1
    case topRight = 2
    case bottomLeft = 3
    case bottomRight = 4
    
    public var description: String {
        switch self {
        case .topLeft:
            "topLeft"
        case .topRight:
            "topRight"
        case .bottomLeft:
            "bottomLeft"
        case .bottomRight:
            "bottomRight"
        }
    }
    
    static func getLocation(from location: String) -> GlanceLocation? {
        switch location {
        case "topleft":
            return .topLeft
        case "topright":
            return .topRight
        case "bottomleft":
            return .bottomLeft
        case "bottomright":
            return .bottomRight
        default:
            print("Must handle new location case.")
            return nil
        }
    }
}

@objc public protocol GlanceDelegate: AnyObject {
    func sessionConnected(dismissAction: (() -> Void)?)
    @available(*, deprecated, renamed: "presenceSessionStarted")
    func presenceConnected(dismissAction: (() -> Void)?)
    func receivedSessionCode(_ sessionCode: String)
    func sessionAgentConnected()
    func onGuestCountChange(guests: [GlanceAgent], response: String?)
    func sessionEnded(_ sessionKey: String?)
    
    func presenceDidConnect()
    func presenceDidDisconnect()
    func presenceSessionStarted()
    
    func receivedVideoCaptureImage(_ image: UIImage)
    func didStartVideoCapture(_ captureSession: AVCaptureSession)
    func didStopVideoCapture(_ captureSession: AVCaptureSession)
    
    func didStartPreviewVideoCapture(_ captureSession: AVCaptureSession)
    func didStopPreviewVideoCapture(_ captureSession: AVCaptureSession)
    
    func didPauseVisitorVideo()
    
    func didStartAgentViewer(_ glanceAgentViewer: UIView)
    func didStopAgentViewer()
    
    func onUpdateVisibility(to visibility: GlanceVisibility)
    func onUpdateLocation(to location: GlanceLocation)
    func onUpdateSize(size: String)
    func askForVideoSession(confirmAction: @escaping (() -> Void), cancelAction: (() -> Void)?)
    func agentAskedForVideoSession()
    func onUpdateAgentVideo()
    
    func initTimeoutExpired()
    func startTimeoutExpired()
    func presenceTimeoutExpired()
    
    func onEvent(_ event: GlanceEvent)
    func onError(_ error: GlanceError)
}

@objc public protocol GlanceHostedDelegate: AnyObject {
    func hostedSessionDidStart(sessionKey: String)
    func hostedSessionDidEnd(errorMessage: String)
}

extension GlanceDelegate {
    func presenceConnected(dismissAction: (() -> Void)?) {}
}

@objc public final class Glance: NSObject {
    public var videoSession: GlanceVideoSession?
    public var previewCameraManager: SessionUICameraManager?
    @objc public weak var delegate: GlanceDelegate?
    
    @objc public static var shared = Glance()
    
    @objc public var initParams: GlanceVisitorInitParams?
    @objc public var startParams: GlanceStartParams?
    
    @objc public weak var hostedSessionDelegate: GlanceHostedDelegate?
    
    var hostedUser: GlanceUser?
    var hostedSession: GlanceHostSession?
    var hostedGuestSession: GlanceGuestSession?
    
    let service: MobileSettingsServiceProtocol = MobileSettingsService()
    var mobileSettings: Settings?
    var lastFetchDate: Date?
    var fetchSettingsInterval: SettingsInterval = .never
    var isDefaultPresenceIntegrationEnabled: Bool = true
    var isDefaultTwoWayVideoIntegration: Bool = true
    var isDefaultRenderModeIntegration: Bool = true
    var cancellables = Set<AnyCancellable>()
    
    var webSocket: GlanceWebSocket?
    
    var glanceVideoSessionKey = ""
    var glanceVideoSessionType = ""
    var glanceVideoMode: GlanceVideoMode?
    var isPresenceOn = false
    var sessionStarted = false
    var presenceStart = false
    var isHostedActive = false
    var guestcount = 0
    var isScreensharePaused = false
    
    var initTimeout: Double?
    var startTimeout: GlanceTimeout?
    var presenceTimeout: Double?
    var maxPresenceConnectAttempts: Int32?
    var maxAgentSessionConnectAttempts: Int?
    var agentSessionConnectionFailCount: Int = 0
    
    var initTimeoutExpired: Bool = false
    var startTimeoutExpired: Bool = false
    var presenceTimeoutExpired: Bool = false
    
    var initTimeoutWorkItem: DispatchWorkItem?
    var startTimeoutWorkItem: DispatchWorkItem?
    var presenceTimeoutWorkItem: DispatchWorkItem?
    var captureSession: AVCaptureSession?
    var previewCaptureSession: AVCaptureSession?
    
    @objc public override init() {
        super.init()
        GlanceVisitor.add(self)
        GlanceVisitor.setCustomViewerDelegate(self)
        Task {
            await webSocket = GlanceWebSocket()
        }
    }
    
    public static func getCallId()-> UInt {
        return GlanceCore.getCallId();
    }
    
    public static func initVisitor(
        params: GlanceVisitorInitParams,
        timeout: Double? = nil,
        fetchSettingsInterval: SettingsInterval = .never,
        isDefaultPresenceIntegrationEnabled: Bool = true,
        isDefaultTwoWayVideoIntegration: Bool = true,
        isDefaultRenderModeIntegration: Bool = true
        
    ) {
        shared.initTimeout = timeout
        shared.initVisitor(params: params)
        shared.fetchSettingsInterval = fetchSettingsInterval
        shared.isDefaultPresenceIntegrationEnabled = isDefaultPresenceIntegrationEnabled
        shared.isDefaultTwoWayVideoIntegration = isDefaultTwoWayVideoIntegration
        shared.isDefaultRenderModeIntegration = isDefaultRenderModeIntegration
    }
    
    public static func startSession(
        params: GlanceStartParams,
        timeout: GlanceTimeout? = nil,
        maxConnectAttempts: Int? = nil
    ) {
        shared.startTimeout = timeout
        shared.maxAgentSessionConnectAttempts = maxConnectAttempts
        shared.startSession(params: params)
    }
    
    public static func startVideoPreview() {
        shared.startCaptureSession()
    }
    
    public static func stopVideoPreview() {
        shared.stopCaptureSession()
    }
    
    public static func startVideoSession() {
        shared.startVideoSession()
    }
    
    public static func setDelegate(_ delegate: GlanceDelegate) {
        shared.delegate = delegate
    }
    
    public static func endSession() {
        Glance.shared.sessionStarted = false
        shared.endVideoSession()
        GlanceCore.endSession()
    }
    
    public static func setPresence(
        _ isPresenceOn: Bool,
        timeout: Double? = nil,
        maxConnectAttempts: Int32? = nil
    ) {
        shared.isPresenceOn = isPresenceOn
        shared.presenceTimeout = timeout
        shared.maxPresenceConnectAttempts = maxConnectAttempts
    }
    
    public static func pausePresence(isPaused: Bool) {
        if shared.isPresenceOn && isPaused {
            shared.disconnectPresence()
        }
        shared.isPresenceOn = !isPaused
        
        if !isPaused, let initParams = shared.initParams {
            shared.initVisitor(params: initParams)
        }
    }
    
    public static func setReplayKitEnabled(_ enabled:Bool){
        GlanceVisitor.setReplayKitEnabled(enabled)
    }
    
    public static func setDrawViewHierarchy(_ enabled: Bool){
        Glance.shared.validateAvaibilityToDrawViewHierarchy(enabled)
    }
    
    private func updateRenderMethod(_ enabled: Bool) {
        let key = "DCapDrawViewHierarchyInRect"
        let value:Int = enabled ? 1 : 0
        let applicationID = "com.glancenetworks.Glance"
        if let defaults = UserDefaults(suiteName: applicationID) {
            defaults.set(value, forKey: key)
            defaults.synchronize()
        }
        GlanceCore.updateRenderMethod()
    }
    
    public static func send(message: String, properties: String) {
        GlanceCore.sendUserMessage(message, properties: properties)
    }
    
    public static func maskKeyboard(_ mask: Bool) {
        GlanceCore.maskKeyboard(mask)
    }
    
    public static func addMaskedView(_ view: UIView, withLabel: String) {
        GlanceCore.addMaskedView(view, withLabel: withLabel)
    }
    
    public static func removeMaskedView(_ view: UIView) {
        GlanceCore.removeMaskedView(view)
    }
    
    public static func isPaused() -> Bool {
        shared.isScreensharePaused
    }
    
    public static func pause(_ isPaused: Bool, message: String? = nil) {
        let maxLength = 1000
        let reducedMessage = message?.reduceString(toLength: maxLength)
        GlanceVisitor.setGlancePauseMessage(reducedMessage)
        shared.isScreensharePaused = isPaused
        GlanceCore.pause(shared.isScreensharePaused)
    }
    
    public static func togglePause() {
        shared.isScreensharePaused.toggle()
        GlanceCore.pause(shared.isScreensharePaused)
    }
    
    func initVisitor(params: GlanceVisitorInitParams) {
        initParams = params
        initParams?.cameras = "\"front\",\"back\""
        initParams?.site = "production"
        
        startParams = GlanceStartParams()
        startParams?.key = initParams?.visitorid
        if let initParams {
            startParams?.displayParams = initParams.displayParams
        }
        if let initParams {
            GlanceCore.setup(initParams)
            
            initTimeoutExpired = false
            setupInitTimeout()
        }
    }
    
    func startSession(params: GlanceStartParams) {
        if params.key == nil {
            return
        }
        
        guard delegate != nil else { return }
        
        self.sessionStarted = true
        startParams = params
        startParams?.displayParams.displayName = ""
        if let glanceVideoMode {
            startParams?.video = glanceVideoMode
        }
        validateAvaibilityToStartNewSession()
    }
    
    func validateAvaibilityToStartNewSession() {
        guard let groupid = self.initParams?.groupid else { return }
        if fetchSettingsInterval == .never  {
            startNewSession()
        } else if let video = startParams?.video, video == VideoSmallMultiway || video == VideoLargeMultiway {
            Glance.fetchSettingsIfNeeded(groupID: String(groupid), platform: "ios", visitor: "visitor") { [weak self] in
                guard let self else { return }
                if mobileSettings?.ios.twoWayVideoIntegration ?? isDefaultTwoWayVideoIntegration {
                    startNewSession()
                } else {
                    // twoWayVideoIntegration is off or doesn't exist
                    debugPrint("Two Way video is not available")
                }
            }
        } else {
            startNewSession()
        }
    }
    
    func startNewSession() {
        GlanceCore.startSession(startParams)
        startTimeoutExpired = false
        setupStartTimeout()
    }
    
    func startVideoSession() {
        guard let screenshareSessionKey = startParams?.key else { return }
        
        if startParams?.video == VideoOff {
            startParams?.video = VideoSmallMultiway
        }
        glanceVideoMode = startParams?.video
        
        let random4DigitNumber = Int.random(in: 1000...9999)
        glanceVideoSessionKey = "\(screenshareSessionKey)V\(random4DigitNumber)"
        glanceVideoSessionType = "visitorVideo"
        
        GlanceCore.startVideoCall()
        
        let videoSize = (glanceVideoMode == VideoLargeVisitor || glanceVideoMode == VideoLargeMultiway) ? "large" : "small"
        let videoMultiway = (glanceVideoMode == VideoSmallMultiway || glanceVideoMode == VideoLargeMultiway) ? "on" : "off"
        let videoSessionProperties = String(format: "size:%@;multiway:%@", videoSize, videoMultiway)
        
        GlanceCore.startVideoSession(videoSessionProperties, sessionType: glanceVideoSessionType, sessionKey: glanceVideoSessionKey)
    }
    
    func startVideoStreaming() {
        guard let groupId = initParams?.groupid else { return }
        
        let videoStartParams: GlanceStartParams = GlanceStartParams()
        videoStartParams.mainCallId = GlanceCore.getCallId()
        videoStartParams.key = glanceVideoSessionKey
        videoStartParams.video = glanceVideoMode ?? VideoOff
        
        videoStartParams.displayParams.displayName = "front"
        videoStartParams.displayParams.scale = 1
        videoStartParams.displayParams.captureWidth = startParams?.displayParams.captureWidth ?? 0
        videoStartParams.displayParams.captureHeight = startParams?.displayParams.captureHeight ?? 0
        
        videoSession = GlanceVideoSession(groupId: groupId, delegate: self)
        
        videoSession?.start(videoStartParams)
    }
    
    @objc public func startCaptureSession() {
        previewCameraManager = SessionUICameraManager(delegate: self)
        previewCameraManager?.startCaptureSession()
    }
    
    @objc public func stopCaptureSession() {
        previewCameraManager?.endCaptureSession()
        previewCaptureSession?.stopRunning()
    }
    
    func endVideoSession() {
        videoSession?.end()
        GlanceCore.endVideoSession()
    }
    
    func disconnectPresence() {
        GlancePresenceVisitor.disconnect()
        Task {
            await webSocket?.disconnect()
        }
    }
    
    func setupInitTimeout() {
        guard let initTimeout else { return }
        
        let workItem = DispatchWorkItem(block: { [weak self] in
            self?.initTimeoutExpired = true
            self?.delegate?.initTimeoutExpired()
        })
        
        DispatchQueue.main.asyncAfter(deadline: .now() + initTimeout, execute: workItem)
        initTimeoutWorkItem = workItem
    }
    
    func setupStartTimeout() {
        guard let startTimeout else { return }
        
        let workItem = DispatchWorkItem(block: { [weak self] in
            self?.startTimeoutExpired = true
            Glance.endSession()
            self?.delegate?.startTimeoutExpired()
        })
        
        DispatchQueue.main.asyncAfter(deadline: .now() + startTimeout.timeInSeconds, execute: workItem)
        startTimeoutWorkItem = workItem
    }
    
    func setupPresenceTimeout() {
        guard let presenceTimeout else { return }
        
        let workItem = DispatchWorkItem(block: { [weak self] in
            self?.presenceTimeoutExpired = true
            self?.disconnectPresence()
            self?.delegate?.presenceTimeoutExpired()
        })
        
        DispatchQueue.main.asyncAfter(deadline: .now() + presenceTimeout, execute: workItem)
        presenceTimeoutWorkItem = workItem
    }
    
    private func sendPresenceEvent(message: WSMessage, data: [String: String] = [:]) {
        Task {
            if await webSocket?.isConnected == true {
                await webSocket?.send(message: message, data: data)
            }
            
            if GlanceCore.isPresenceConnected() {
                GlancePresenceVisitor.signalAgent(message.rawValue, map: data)
            }
        }
    }
}

// MARK: - Events

extension Glance: GlanceVisitorDelegate {
    public func glanceVisitorEvent(_ event: GlanceEvent) {
        defer {
            delegate?.onEvent(event)
        }
        
        let event: GlanceSDKEvent = .init(event)
        switch event.code {
        case .EventVisitorInitialized:
            guard !initTimeoutExpired else { return }
            initTimeoutWorkItem?.cancel()
            
            DispatchQueue.main.async { [weak self] in
                guard let self else { return }
                if self.isPresenceOn {
                    if fetchSettingsInterval == .never {
                        GlancePresenceVisitor.connect(self, maxAttempts: maxPresenceConnectAttempts ?? -1)
                        if let initParams {
                            Task {
                                await self.webSocket?.setup(startParams: initParams)
                            }
                        }
                    } else {
                        guard let groupid = self.initParams?.groupid else { return }
                        Glance.fetchSettingsIfNeeded(groupID: String(groupid), platform: "ios", visitor: "visitor") { [weak self] in
                            guard let self else { return }
                            if mobileSettings?.ios.presenceIntegration ?? isDefaultPresenceIntegrationEnabled {
                                GlancePresenceVisitor.connect(self, maxAttempts: maxPresenceConnectAttempts ?? -1)
                                if let initParams {
                                    Task {
                                        await self.webSocket?.setup(startParams: initParams)
                                    }
                                }
                            } else {
                                // presenceIntegration is off or doesn't exist
                                debugPrint("Presence Integration is not available")
                                self.delegate?.presenceDidDisconnect()
                                GlancePresenceVisitor.disconnect()
                                if let initParams {
                                    Task {
                                        await self.webSocket?.disconnect()
                                    }
                                }
                            }
                        }
                    }
                } else {
                    GlancePresenceVisitor.disconnect()
                    if let initParams {
                        Task {
                            await self.webSocket?.disconnect()
                        }
                    }
                }
                self.setupPresenceTimeout()
            }
        case .EventConnectedToSession:
            agentSessionConnectionFailCount = 0
            let sessionKey = event.properties["sessionkey"] as? String ?? ""
            let videoMode = event.properties["video"] as? String ?? ""
            let presencestart = (event.properties["presencestart"] as? NSString)?.boolValue ?? false
            presenceStart = presencestart
            
            if (!presencestart && (startTimeout?.type == .network && startTimeoutExpired)) {
                return
            }
            
            if startTimeout?.type == .network {
                startTimeoutWorkItem?.cancel()
            }
            
            if videoMode != "videooff" && videoMode != "off" && videoMode != "" {
                glanceVideoMode = videoMode == "large" || videoMode == "largemultiway" ? VideoLargeMultiway : VideoSmallMultiway
                startVideoSession()
            } else {
                glanceVideoMode = VideoOff
            }
            
            if guestcount > 0 {
                delegate?.sessionAgentConnected()
            } else if !presencestart {
                delegate?.receivedSessionCode(sessionKey)
            }
        case .EventSessionEnded:
            let sessionKey = event.properties["ssnkey"] as? String
            captureSession?.stopRunning()
            sessionStarted = false
            glanceVideoMode = nil
            guestcount = 0
            Glance.endSession()
            delegate?.sessionEnded(sessionKey)
            debugPrint("EventSessionEnded - sessionkey: \(sessionKey ?? "")")
        case .EventGuestCountChange:
            guard sessionStarted else { return }
            guestcount = Int(event.properties["guestcount"] as? String ?? "0") ?? 0
            let guestList = event.properties["guestlist"] as? String
            var guests = [GlanceAgent]()
            
            if let jsonData = guestList?.data(using: .utf8) {
                do {
                    guests = try JSONDecoder().decode([GlanceAgent].self, from: jsonData)
                    // Remove the guest with Agent role visitor
                    guests = guests.filter { $0.role != "visitor" }
                } catch let error {
                    print(error)
                }
            }
            
            if guestcount > 0 && !(startTimeout?.type == .default && startTimeoutExpired) {
                if startTimeout?.type == .default {
                    startTimeoutWorkItem?.cancel()
                }
                delegate?.sessionAgentConnected()
            }
            
            delegate?.onGuestCountChange(guests: guests, response: guestList)
        case .EventMessageReceived:
            guard sessionStarted else { return }
            let message = event.properties["message"] as? String ?? ""
            
            if message == "widgetlocation" {
                let location = event.properties["location"] as? String ?? ""
                let containerLocation: GlanceLocation? = GlanceLocation.getLocation(from: location)
                
                if let containerLocation {
                    delegate?.onUpdateLocation(to: containerLocation)
                }
            } else if message == "widgetvisibility" {
                let visibility = event.properties["visibility"] as? String ?? ""
                
                if visibility == "tab" {
                    delegate?.onUpdateVisibility(to: .tab)
                } else {
                    delegate?.onUpdateVisibility(to: .full)
                }
            } else if message == "pausevisitorvideo" {
                let paused = event.properties["paused"] as? String ?? ""
                
                if paused == "true" {
                    delegate?.didPauseVisitorVideo()
                }
            } else if message == "visitorvideorequested" {
                if glanceVideoMode == VideoOff {
                    Task { @MainActor in delegate?.agentAskedForVideoSession() }
                } else if glanceVideoMode == VideoSmallVisitor {
                    startParams?.video = VideoSmallMultiway
                    glanceVideoMode = startParams?.video
                    delegate?.onUpdateAgentVideo()
                    
                    if let startParams {
                        endVideoSession()
                        startVideoStreaming()
                        DispatchQueue.global().asyncAfter(deadline: .now() + 0.25) { [weak self] in
                            self?.startVideoSession()
                        }
                    }
                }
            } else if message == "videosize" {
                let size = event.properties["size"] as? String ?? ""
                delegate?.onUpdateSize(size: size)
            }
        case .EventPresenceConnected:
            delegate?.presenceDidConnect()
            GlancePresenceVisitor.presence(["url": "root view"])
            presenceTimeoutWorkItem?.cancel()
        case .EventPresenceDisconnected:
            delegate?.presenceDidDisconnect()
        case .EventChildSessionStarted:
            startVideoStreaming()
        case .EventChildSessionEnded:
            print("Child ended")
        case .EventStartSessionFailed:
            guard let maxAgentSessionConnectAttempts, maxAgentSessionConnectAttempts >= 0 else { return }
            if agentSessionConnectionFailCount < maxAgentSessionConnectAttempts  {
                validateAvaibilityToStartNewSession()
                agentSessionConnectionFailCount += 1
            }
        case .EventPresenceStartSession, .EventPresenceStartVideo:
            let videoMode = event.properties["video"] as? String ?? ""
            if videoMode == "large" {
                glanceVideoMode = VideoLargeMultiway
            } else if videoMode != "videooff" && videoMode != "off" && videoMode != "" {
                glanceVideoMode = VideoSmallMultiway
            } else {
                glanceVideoMode = VideoOff
            }
            
            sendPresenceEvent(message: .visitorsessionstart)
            sessionStarted = true
            
            delegate?.sessionConnected {
                Glance.endSession()
            }
            
            delegate?.presenceConnected {
                Glance.endSession()
            }
            
            delegate?.presenceSessionStarted()
        case .EventPresenceShowTerms:
            guard !sessionStarted else  { return }
            let videoMode = event.properties["video"] as? String ?? ""
            if videoMode == "large" {
                glanceVideoMode = VideoLargeMultiway
            } else if videoMode != "videooff" && videoMode != "off" && videoMode != "" {
                glanceVideoMode = VideoSmallMultiway
            } else {
                glanceVideoMode = VideoOff
            }
            
            sessionStarted = false
            DispatchQueue.main.async { [weak self] in
                guard let self else { return }
                self.sendPresenceEvent(message: .terms, data: ["status": "displayed"])
                
                self.delegate?.askForVideoSession(
                    confirmAction: {
                        self.sendPresenceEvent(message: .terms, data: ["status": "accepted"])
                        self.sendPresenceEvent(message: .visitorsessionstart)
                        self.sessionStarted = true
                        
                        self.delegate?.sessionConnected {
                            Glance.endSession()
                        }
                        
                        self.delegate?.presenceConnected {
                            Glance.endSession()
                        }
                        
                        self.delegate?.presenceSessionStarted()
                    },
                    cancelAction: {
                        GlancePresenceVisitor.signalAgent("terms", map: ["status": "declined"])
                        self.webSocket?.send(message: .terms, data: ["status": "declined"])
                        self.sessionStarted = false
                        self.guestcount = 0
                    }
                )
            }
        default:
            #if canImport(GlanceSDK)
                print("Must handle new case code: \(event.code)\n \(event.message)")
            #endif
        }
        if event.type == .error || event.type == .assertFail {
            showErrorOrFailureDialog(event: event)
        }
    }
    
    private func showErrorOrFailureDialog(event: GlanceSDKEvent) {
        let error = GlanceError(code: event.code, message: event.message)
        delegate?.onError(error)
    }
    
    private func validateAvaibilityToDrawViewHierarchy(_ enabled: Bool) {
        if fetchSettingsInterval == .never  {
            updateRenderMethod(enabled)
        } else {
            guard let groupid = self.initParams?.groupid else { return }
            Glance.fetchSettingsIfNeeded(groupID: String(groupid), platform: "ios", visitor: "visitor") { [weak self] in
                guard let self else { return }
                if let renderMode = mobileSettings?.ios.renderModeIntegration {
                    switch renderMode {
                    case .renderInContext:
                        updateRenderMethod(false)
                    case .drawViewInHierarchy:
                        updateRenderMethod(true)
                    }
                } else if isDefaultRenderModeIntegration {
                    updateRenderMethod(enabled)
                } else {
                    // renderModeIntegration is off or doesn't exist
                    debugPrint("Render Mode is not available")
                }
            }
        }
    }
}

extension Glance: GlanceVideoSessionDelegate {
    public func glanceVideoSessionDidStart(_ session: GlanceVideoSession!) {
        print("glanceVideoSessionDidStart")
    }
    
    public func glanceVideoSessionDidStartVideoCapture(_ session: GlanceVideoSession!) {
        print("glanceVideoSessionDidStartVideoCapture")
    }
    
    public func glanceVideoSessionDidConnectVideoSource(_ session: GlanceVideoSession!) {
        print("glanceVideoSessionDidConnectVideoSource")
    }
    
    public func glanceVideoSessionDidDisconnectVideoSource(_ session: GlanceVideoSession!) {
        print("glanceVideoSessionDidDisconnectVideoSource")
    }
    
    public func glanceVideoSessionDidFailToConnectVideoSource(_ session: GlanceVideoSession!, error: Error!) {
        print("glanceVideoSessionDidFailToConnectVideoSource")
    }
    
    public func sessionUICameraManagerDidCapture(_ image: UIImage!) {
        delegate?.receivedVideoCaptureImage(image)
    }
    
    public func glanceVideoSessionDidConnectStreamer(_ session: GlanceVideoSession!) {
        print("glanceVideoSessionDidConnectStreamer")
    }
    
    public func glanceVideoSessionDidDisconnnectStreamer(_ session: GlanceVideoSession!) {
        print("glanceVideoSessionDidDisconnnectStreamer")
    }
    
    public func glanceVideoSessionWillStartStreaming(_ session: GlanceVideoSession!) {
        if !(captureSession?.isRunning ?? false) {
            captureSession = session.captureSession
            delegate?.didStartVideoCapture(session.captureSession)
        }
    }
    
    public func glanceVideoSessionWillStopStreaming(_ session: GlanceVideoSession!) {
        if !(captureSession?.isRunning ?? false) {
            captureSession = session.captureSession
            delegate?.didStopVideoCapture(session.captureSession)
        }
    }
    
    public func glanceVideoSessionDidFailConnectStreamer(_ session: GlanceVideoSession!, error: Error!) {
        print("glanceVideoSessionDidFailConnectStreamer")
    }
    
    public func glanceVideoSessionWillChangeQuality(_ session: GlanceVideoSession!) {
        print("glanceVideoSessionWillChangeQuality")
    }
    
    public func glanceVideoSessionDidEnd(_ session: GlanceVideoSession!) {
        print("glanceVideoSessionDidEnd")
    }
    
    public func glanceVideoSessionInvitation(_ session: GlanceVideoSession!, sessiontype: String!, username: String!, sessionkey sesionkey: String!) {
        print("glanceVideoSessionInvitation")
    }
}

extension Glance: SessionUICameraManagerDelegate {
    public func sessionUICameraManagerDidStartVideoCapture(_ instance: SessionUICameraManager) {
        guard let captureSession = instance.captureSession else { return }
        self.previewCaptureSession = captureSession
        delegate?.didStartPreviewVideoCapture(captureSession)
    }
    
    public func sessionUICameraManagerDidStopVideoCapture(_ instance: SessionUICameraManager) {
        guard let captureSession = instance.captureSession else { return }
        self.previewCaptureSession = captureSession
        delegate?.didStopPreviewVideoCapture(captureSession)
    }
}

extension Glance: SessionUIDelegate {
    public func sessionUIVoiceAuthenticationRequired() {}
    public func sessionUIVoiceAuthenticationFailed() {}
    public func sessionUIDidError(_ error: Error!) {}
    public func sessionUIDialogAccepted() {}
    public func sessionUIDialogCancelled() {}
}

extension Glance: GlanceCustomViewerDelegate {
    public func glanceViewerDidStart(_ glanceView: UIView, size: CGSize) {
        print("glanceViewerDidStart")
    }
    
    public func glanceViewerDidStop(_ glanceView: UIView) {
        delegate?.didStopAgentViewer()
    }
    
    public func glanceViewerIsStopping(_ glanceView: UIView) {
        delegate?.didStopAgentViewer()
    }
    
    public func glanceViewerIsStarting(_ glanceView: UIView) {
        delegate?.didStartAgentViewer(glanceView)
    }
}
