//
//  IOS.swift
//  GlanceSDK
//
//  Created by bruno on 12/19/24.
//

enum RenderMode: String, Codable {
    case renderInContext = "RENDER_IN_CONTEXT"
    case drawViewInHierarchy = "DRAW_VIEW_IN_HIERARCHY"
}

struct IOS: Codable {
    let presenceIntegration: Bool
    let userEventCapturing: Bool
    let renderModeIntegration: RenderMode
    let twoWayVideoIntegration: Bool
    let developer: Developer
    
    struct Developer: Codable {
        let betaFeatures: Bool
    }
}
