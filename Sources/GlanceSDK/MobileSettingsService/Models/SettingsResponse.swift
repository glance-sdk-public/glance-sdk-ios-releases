//
//  SettingsResponse.swift
//  GlanceSDK
//
//  Created by bruno on 12/19/24.
//

struct SettingsResponse: Codable {
    let results: Settings
}

struct Settings: Codable {
    let ios: IOS
    let visitor: Visitor
}
