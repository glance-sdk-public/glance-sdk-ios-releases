//
//  MobileSettingsService.swift
//  GlanceSDK
//
//  Created by bruno on 12/18/24.
//

import Foundation
import Combine
#if canImport(GlanceFramework)
import GlanceFramework
#else
import GlanceiOSNetwork
#endif

protocol MobileSettingsServiceProtocol {
    func fetchSettings(
        groupID: String,
        platform: String,
        visitor: String
    ) -> AnyPublisher<SettingsResponse, Error>
}

final class MobileSettingsService: APIRequest, MobileSettingsServiceProtocol {
    func fetchSettings(
        groupID: String,
        platform: String,
        visitor: String
    ) -> AnyPublisher<SettingsResponse, Error> {
        self.fetchRequest(
            target: MobileSettingsAPI.fetchSettings(
                groupID,
                platform,
                visitor
            ),
            dataType: SettingsResponse.self
        )
        .eraseToAnyPublisher()
    }
}
