//
//  GlanceAgent.swift
//
//
//  Created by Felipe Melo on 11/04/24.
//

import Foundation

@objc public class GlanceAgent: NSObject, Decodable {

    public var agentRole: String?
    public var name: String?
    public var partnerId: Int?
    public var partnerUserId: String?
    public var role: String?
    public var title: String?
    public var username: String?

    enum CodingKeys: String, CodingKey {
        case agentRole = "agentrole"
        case name
        case partnerId = "partnerid"
        case partnerUserId = "partneruserid"
        case role
        case title
        case username
    }
    private static func decodeStringOrInt(from container: KeyedDecodingContainer<CodingKeys>, forKey key: CodingKeys) -> String? {
        if let stringValue = try? container.decodeIfPresent(String.self, forKey: key) {
            return stringValue
        }
        else if let intValue = try? container.decodeIfPresent(Int.self, forKey: key) {
            return String(describing: intValue)
        }
        return nil
    }

    
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        agentRole = GlanceAgent.decodeStringOrInt(from: container, forKey: .agentRole)
        name = GlanceAgent.decodeStringOrInt(from: container, forKey: .name )
        partnerId = try container.decodeIfPresent(Int.self, forKey: .partnerId)
        partnerUserId = GlanceAgent.decodeStringOrInt(from: container, forKey: .partnerUserId)
        role = GlanceAgent.decodeStringOrInt(from: container, forKey: .role )
        title = GlanceAgent.decodeStringOrInt(from: container, forKey: .title )
        username = GlanceAgent.decodeStringOrInt(from: container, forKey: .username )
    }
}
