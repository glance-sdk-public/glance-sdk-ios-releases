//
//  BaseURLs.swift
//  GlanceSDK
//
//  Created by bruno on 12/19/24.
//

public enum BaseURLs: String {
    case settings = "https://dev-mobile-settings.myglance.org/api/sdk"
}

