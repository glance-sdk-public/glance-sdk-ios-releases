//
//  MaskedViewModifier.swift
//
//
//  Created by bruno on 4/25/24.
//

import GlanceCore
import SwiftUI

struct MaskedViewModifier: ViewModifier {
    let view: UIView
    let label: String
    
    func body(content: Content) -> some View {
        content
            .overlay(MaskedView(view: view, label: label))
    }
}

extension View {
    public func addMaskedView(withLabel label: String) -> some View {
        let controller = UIHostingController(rootView: self)
        return modifier(MaskedViewModifier(view: controller.view, label: label))
    }
}

struct MaskedView: UIViewRepresentable {
    let view: UIView
    let label: String
    
    func makeUIView(context: Context) -> UIView {
        GlanceCore.addMaskedView(view, withLabel: label)
        return view
    }
    
    func updateUIView(_ uiView: UIView, context: Context) {}
}
